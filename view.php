<?php
    $mode = $_POST['mode'];
    $name = $_POST['username'];
    $pass = $_POST['password'];
    
    session_start();
    
    $url_middle = "https://afsaccess4.njit.edu/~edd8/";
    // $url_middle = "https://afsaccess4.njit.edu/~abh7/";

    proccess_mode($mode);

    function proccess_mode($mode){
        switch($mode) {
            case 'login-user':
                login_user();
                break;
            case 'get-student-landing':
                spawn_student_landing();
                break;
            case 'get-teacher-landing':
                spawn_teacher_landing();
                break;
            case 'list-questions':
                list_questions($mode);
                break;
            case 'get-question':
                get_question($mode);
                break;
            case 'filter':
                filter_questions($mode);
                break;
            case 'add-question':
                add_question($mode);
                break;
            case 'create-unposted-exam':
                add_exam($mode);
                break;
            case 'delete-question':
                delete_question($mode);
                break;
            case 'list-exams':
                list_exams($mode);
                break;
            case 'get-exam':
                get_exam($mode);
                break;
            case 'delete-exam':
                delete_posted_exam($mode);
                break;
            case 'pue':
                post_ungraded_exam($mode);
                break;
            case 'submit-exam':
                submit_ungraded_exam($mode);
                break;
            case 'do-exam':
                do_exam($mode);
                break;
            case 'auto-grade-exam':
                auto_grade_exam($mode);
                break;
            case 'grab-ungraded-exam':
                grab_ungraded_exam($mode);
                break;
            case 'pge':
                post_graded_exam($mode);
                break;
            default:
                return v_throwError("mode '$mode' not implemented in view.php.");
        }
    }

    function call_middle(&$fields){
        global $url_middle;

        $fields_string="";

        global $mode;
        if ($mode != 'login-user'){
            $fields['username']=$_SESSION['username'];
            $fields['password']=$_SESSION['password'];    
        }
        foreach($fields as $key=>$value) { 
            $fields_string .= $key.'='.$value.'&';
        }
        rtrim($fields_string, '&');
    
        $curl_back = curl_init();
        curl_setopt($curl_back, CURLOPT_URL, $url_middle);
        curl_setopt($curl_back, CURLOPT_POST, count($fields));
        curl_setopt($curl_back, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_back, CURLOPT_RETURNTRANSFER, true);
    
        $response = curl_exec($curl_back);
    
        if($response === FALSE){
            die(curl_error($curl_back));
        }
        
        curl_close($curl_back);

        return $response;
    }

    function call_middle_ignore_backend_pipeline(&$fields){
        return json_encode(call_middle($fields));
    }

    function login_user(){
        global $name, $pass;

        $fields = array(
            'mode' => 'login-user',
            'username' => $name,
            'password' => $pass
        );
        $response = json_decode(call_middle($fields));
        $status = json_decode($response)->status; // wow, have to double decode, how stupid.
        
        if($status === 1){
            $_SESSION["isLoggedIn"]="true";
            $_SESSION["username"]=$name;
            $_SESSION["password"]=$pass;
        } else if ($status === 0){
            $_SESSION["isLoggedIn"]="false";
        } else {
            $_SESSION["isLoggedIn"]=$status;
        }
        echo $response;
    }

    function list_questions($mode){
        $fields = array(
            'mode' => $mode
        );
        $response = json_decode(call_middle($fields));
        echo $response;
        return;
    }

    function list_exams($mode){
        $fields = array(
            'mode' =>  $mode
        );
        $response = json_decode(call_middle($fields));

        echo $response;
    }

    function get_exam($mode){
        $fields = array(
            'mode' =>  $mode,
            'global-id'=> $_POST['global-id']
        );
        $response = json_decode(call_middle($fields));

        echo $response;
    }

    function get_question($mode){
        $fields = array(
            'mode' => $mode,
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'id' => $_POST['id']
        );
        $response = json_decode(call_middle($fields));

        echo $response;
        return;
    }

    function filter_questions($mode){
        $d=$_POST['difficulty'] == 'any'?'':$_POST['difficulty'];
        $fields = array(
            'mode' => $mode,
            'difficulty' => $d,
            'topic' => $_POST['topic'],
            'keyword' => $_POST['keyword']
        );
        $response = json_decode(call_middle($fields));
        echo $response;
    }

    function add_question($mode){

        $diff = $_POST['difficulty'];
        if($diff=='easy')$diff=1;
        else if($diff=='medium')$diff=2;
        else if($diff=='hard')$diff=3;

        $grades_fname = array(
            'given'=>'',
            'expected'=> $_POST['function-name'],
            'auto'=>'',
            'manual'=>'',
            'comment'=>''
        );
        $grades_constraint = array(
            'given'=>'',
            'expected'=> $_POST['constraint'],
            'auto'=>'',
            'manual'=>'',
            'comment'=>''
        );

        $gradable_list = array(
            'function-name' => $grades_fname,
            'constraint' => $grades_constraint,
            'tcsp-list'=> json_decode($_POST['tcsp'])
        );

        $fields = array(
            'mode' => $mode,
            'question-name' => $_POST['question-name'],
            'desc' => $_POST['desc'],
            'gradable-list'=>json_encode($gradable_list),
            'difficulty' => $diff,
            'topic' => $_POST['topic']
        );
        // echo json_encode( $fields);
        $response = json_decode(call_middle($fields));
        
        echo $response;
    }

    function add_exam($mode){
        $fields = array(
            'mode' => 'cue',
            'exam-name' => $_POST['exam-name'],
            'questionID-point-pairs' => $_POST['questionID-point-pairs'],
        );
        // v_throwError($fields);// delete this and uncomment the ones below.
        $response = json_decode(call_middle($fields));

        echo $response;
    }

    function post_ungraded_exam($mode){
        $fields = array(
            'mode' => $mode,
            'global-id' =>$_POST['global-id'],
            'name' => $_POST['name']
        );
        $response = json_decode(call_middle($fields));

        echo $response;
    }

    function submit_ungraded_exam($mode){
        $fields = array(
            'mode' => 'mue',
            'role' => 'student',
            'global-id' =>$_POST['exam-id'],
            'mue-data' =>$_POST['exam-data'],
            'submitted-exam' => 'true'
        );
        // v_throwError($mode." :: " .$_POST['exam-id']." :: ".$_POST['exam-data']);
        // return;
        $response = json_decode(call_middle($fields));

        echo $response;
    }

    function post_graded_exam($mode){
        $fields = array(
            'mode' => $mode,
            'role' => 'teacher',
            'global-id' =>$_POST['exam-id'],
            'grade-tuple' =>$_POST['grade-tuple'],
        );
        // v_throwError($mode." :: " .$_POST['exam-id']." :: ".$_POST['exam-data']);
        // return;
        $response = json_decode(call_middle($fields));

        echo $response;
    }
    
    function auto_grade_exam(){
        $fields = array(
            'mode' => 'autograde',
            'role' => 'teacher',
            'qr_pair' =>$_POST['qr-pair'],
        );
        // v_throwError("problem???");
        // return;
        $response = json_decode(call_middle_ignore_backend_pipeline($fields));

        echo $response;
    }

    function delete_question($mode){
        $fields = array(
            'mode' => $mode,
            'id'=> $_POST['id']
        );
        $response = json_decode(call_middle($fields));
        echo $response;
    }

    function delete_posted_exam($mode){
        $fields = array(
            'mode' => $mode,
            'global-id'=> $_POST['global-id'],
            'delete-spawned'=> $_POST['delete-spawned'],
        );
        // v_throwError("work??? ".$_POST['delete-spawned']);
        // exit();

        $response = json_decode(call_middle($fields));
        echo $response;
    }

    function spawn_teacher_landing(){

        $page_data = '
            <h1>Welcome Teacher</h1><br/><br/>
            <button id="cq" >create question</button><br/><br/>
            <button id="ce">create exam</button><br/><br/>
            <button id="eb">exam bank (unposted exams)</button><br/><br/>
            <button id="ge">gradable exams (posted exams)</button><br/><br/>
            <button onclick="history.back()">Go Back</button>
        ';

        $response = array("page_data"=>$page_data);
        echo json_encode( $response);
    }

    function spawn_student_landing(){

        $page_data = '
            <h1>Welcome Student</h1><br/><br/>
            <button id="vge">view graded exam</button><br/><br/>
            <button onclick="history.back()">Go Back</button>
            <div id="spawner"></div><br/><br/>
        ';

        $response = array("page_data"=>$page_data);
        echo json_encode( $response);
    }

    function do_exam($mode){
        
        $exam_target = $_POST['id'];
        
        $_SESSION["ungraded-exam-target"]=$exam_target;
        
        $response = array("status"=>1, "exam-id"=>$exam_target);
        echo json_encode($response);
    }

    function grab_ungraded_exam($mode){
    
        $exam_target = $_SESSION["ungraded-exam-target"];
        
        $response = array(
            "status"=>1, 
            "exam-id"=>$exam_target
        );
        echo json_encode($response);
    }

    function v_throwError($msg){
        $response = array("status"=>0,"desc"=>$msg);
        echo json_encode($response);
    }
    
    exit();
?>