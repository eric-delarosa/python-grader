<?php

        // ctrl K + ctrl # closes -- 4 will close the switch layer.
        // ctrl K + ctrl J opens all

        $db_params = parse_ini_file( dirname(__FILE__).'/priv/db_params.ini',false);
        
        $mydb = new mysqli($db_params['db_server_url'],$db_params['server_username'],$db_params['db_password'],$db_params['db_name']);

        if ($mydb->connect_error){
            die('Connection Failed... :: ' . $mydb->connect_error);
        }

        // this call isnt necessary, but postman was acting up without it.
        if(empty($_SERVER['CONTENT_TYPE']))
        {
            $_SERVER['CONTENT_TYPE'] = "application/x-www-form-urlencoded"; 
        }

        $name = $_POST['username']; // POST as 'username'
        $pass = $_POST['password']; // POST as 'password'
        $role = $_POST['role'];
        $mode = $_POST['mode']; 
            // set-pass     :: requires 'username' and new 'password'
            // create-user  :: requires 'mode' & 'username' & 'role' & 'password'
            // delete-user  :: requires 'mode' & 'username'
            // send-data    :: requires 'mode' & 'username' & 'data'
            // get-data     :: requires 'mode' & 'username'

        $sender_Data = $_POST['data'];

        $debug_mode = $_POST['debug']; // options are true or false.
        $debug_mode2 = $_POST['debug-mode']; // intuitive debug call

        { // add-question API:
            $cq_desc = $_POST['description'];
            /* alias */ if($cq_desc=="") $cq_desc = $_POST['desc'];
            
            $cq_name = $_POST['question-name']; // identifying name for previews
            /* alias */ if($cq_name=="") $cq_name = $_POST['name']; // feel free to give 'name' instead of 'question-name' if you so wish

            $cq_func_name = $_POST['function-name']; // name to check, such as: def doubleIt
            
            $cq_gradeable_list = $_POST['gradeable_list'];
            /* alias */ if($cq_gradeable_list=="") $cq_gradeable_list = $_POST['gradable_list'];
            /* alias */ if($cq_gradeable_list=="") $cq_gradeable_list = $_POST['gradable-list'];
            /* alias */ if($cq_gradeable_list=="") $cq_gradeable_list = $_POST['gradeable-list'];
            /* alias */ if($cq_gradeable_list=="") $cq_gradeable_list = $_POST['gl'];
            /* alias */ if($cq_gradeable_list=="") $cq_gradeable_list = $_POST['grades'];

            $cq_diff = $_POST['difficulty'];
            $cq_topic = $_POST['topic'];
            $cq_constraint = $_POST['constraint'];
            //responds an id.
        }

        { // delete-question
            $dq_id = $_POST['id'];
        }

        { // get-question --> returns array of exam attributes.
            $gq_id = $_POST['id'];
        }

        { // add the following APIs: get-question, create-exam, get-exam, delete-exam, 
            // send-ungraded-exam, get-ungraded-exam, send-graded-exam, get-graded-exam
            // helpers: get-question-by-difficulty, get-question-by-topic, get-question-by-
            
            // list-exams --> returns names, type (graded, ungraded, unposted), and ids. takes no arguments

            // tables:
            /** question_bank: holds question objects identified with id.
             * 
             * unposted_exams ( list of exams - no tables ): 
             *          holds the exam's id, 
             *          the exam's name,
             *          a json'd dictionary of (questionID=>point) pairs. --point is an int
             *     ~ intention note: an unposted exam is effectively a pointer to question ids. on submission, becomes an ungraded exam.
             * 
             * posted_exams ( list of exam table names ):
             *          holds the NAME of the generated TABLE that holds its exam,
             *          the exam's id,
             *          the exam's name (could be the student's name),
             *   ** (generated) its exam table-> for each row:
             *          holds question's bank id,
             *          student's response_data as a varchar(512) --starts off as empty
             *          question's score
             *     ~ intention note: this is what the student sends back. instructor can edit at any time(?any comments?), instructor sets score.
             *  
             *  exam_status ( list of exam table id's ):
             *          holds exam ids
             *          their names,
             *          and their grade type (unposted/posted/graded)
             *    ~ graded exams can not be retrieved as posted.
             */
        }

        { // 'create-unposted-exam' OR 'cue'
            $cue_name = $_POST['exam-name'];
            /* alias */ if($cue_name=="") $cue_name = $_POST['name'];

            $cue_question_point_pairs = $_POST['questionID-point-pairs']; 
            /* alias */ if($cue_question_point_pairs=="") $cue_question_point_pairs = $_POST['qpp'];
        }

        { // get-exam --> expects exam id, NOT name. 
                // should use this call to also setup exams for the student. not just edit as instructor
            $ge_id = $_POST['global-id'];
        }

        { // delete-exam --> returns status.
            $de_id = $_POST['global-id'];
            $de_spawner_exam = $_POST['delete-spawned']; // naming is bad. this is the exam which u want to reset its spawned exam attribute.
        }


        { // post-ungraded-exam OR pue --> returns status
            $pue_id = $_POST['global-id']; //taken from exam list
            $pue_name = $_POST['name'];
            
            // optional call. if empty, global-comment will be set to 'No Comment'
            $pue_global_comment = $_POST['global-comment'];
            /* alias */ if($pue_global_comment=="") $pue_global_comment = $_POST['exam-comment'];
            
        }

        { // modify-ungraded-exam or 'mue' -->
            $mue_name = $_POST['name'];
            /* alias */ if($mue_name=="") $mue_name = $_POST['new-exam-name'];
            $mue_id = $_POST['global-id']; //taken from exam list
            $mue_data = $_POST['exam-overwrite-array'];
            /* alias */ if($mue_data=="") $mue_data = $_POST['exam-data'];
            /* alias */ if($mue_data=="") $mue_data = $_POST['mue-data'];
            $mue_global_comment = $_POST['comment'];
            /* alias */ if($mue_global_comment=="") $mue_global_comment = $_POST['global-comment'];

            $mue_exam_isSubmitted = $_POST['submitted-exam'];
        }
        
        { // post-graded-exam --> returns status and an id.
            $pge_name = $_POST['name'];
            $pge_id = $_POST['global-id']; //taken from exam list
            $pge_grade_tuple = $_POST['grade-tuple']; //taken from exam list
        }

        { // get-questions-by-difficulty / alias: gqbd --> returns a list of question names, ids, and type (ungraded/graded/posted)
            
        }

        { // get-questions-by-topic / alias: gqbt --> returns a list of question names, ids, and type
            
        }

        { // find-questions-by-keyword / alias: gqbk --> returns a list of question names and ids
            $fqbk_keyword = $_POST['keywords'];

            $fqbk_keyword_delimiter = $_POST['delimiter']; // if delimiter is not set, 
            /* alias */ if($fqbk_keyword_delimiter=="") $fqbk_keyword_delimiter = $_POST['delim'];
            /* alias */ if($fqbk_keyword_delimiter=="") $fqbk_keyword_delimiter = $_POST['seperator'];
        }

        { // find-exams-by-keyword / alias: gqbk --> returns a list of question names and ids
            $febk_keyword = $_POST['keywords'];

            $febk_keyword_delimiter = $_POST['delimiter']; // if delimiter is not set, 
            /* alias */ if($febk_keyword_delimiter=="") $febk_keyword_delimiter = $_POST['delim'];
            /* alias */ if($febk_keyword_delimiter=="") $febk_keyword_delimiter = $_POST['seperator'];
        }

        { // filter
            $filter_topic = $_POST['topic'];
            $filter_diff = $_POST['difficulty'];

            $filter_keyword = $_POST['keywords'];
            /* alias */ if($filter_keyword=="") $filter_keyword = $_POST['keyword'];

            $filter_keyword_delimiter = $_POST['delimiter']; // if delimiter is not set, 
            /* alias */ if($filter_keyword_delimiter=="") $filter_keyword_delimiter = $_POST['delim'];
            /* alias */ if($filter_keyword_delimiter=="") $filter_keyword_delimiter = $_POST['seperator'];
        }
        


        if($debug_mode == 'true' || $debug_mode2 == 'true') {
            echo "\n";
            echo $_SERVER['REQUEST_METHOD'];        
            echo "\nname: $name \n";
            echo "pass: $pass \n";
            echo "mode: $mode \n";
        }
        /** usernames (NOT sensitive) :: and passwords (case-sensitive).
         *  Teacher - teacher8 
         *  Student - student8 
         */

        $internal_call = false;


        { //test here
            // $mode='get-question'; $name='teacher'; $pass='teacher8'; $role='teacher'; $gq_id='2';
            // $t=array( array("q"=>10, "p"=>11),array("q"=>20, "p"=>22),array("q"=>30, "p"=>33),array("q"=>40, "p"=>44));
            // $u = json_encode($t);
            // $v = json_decode($u);
            // gettype($v);
            // return;

            // $mode='febk';
            // $fqbk_keyword='second';
            // $fqbk_keyword_delimiter=",";

            // $mode='filter';
            // $filter_diff='hard';
            // $filter_topic='for';
            // $filter_keyword = 'num';
        }

        call_tasks();
    
        function call_tasks() {
            
            global $internal_call;

            {
                global $mode;
                global $mydb;

                global $name, $pass, $role, $mode;
                global $sender_Data;
        
                global $debug_mode, $debug_mode2;
        
                global $cq_desc, $cq_name, $cq_func_name, $cq_gradeable_list, $cq_diff, $cq_topic, $cq_constraint;
                
                global $dq_id;
                global $gq_id;
                global $cue_name, $cue_question_point_pairs;
                global $ge_id;
                global $de_id;
        
                global $pue_id, $pue_name, $pue_global_comment;

                global $mue_name, $mue_id, $mue_data, $mue_global_comment, $mue_exam_isSubmitted;
                global $pge_name, $pge_id, $pge_grade_tuple;

                global $filter_topic, $filter_diff, $filter_keyword, $filter_keyword_delimiter;
            }

            switch($mode){
                case 'set-pass':
                    if($debug_mode == 'true' || $debug_mode2 == 'true') {
                        echo "\n\t\tMode = :: Set Pass :: \n\n";
                    }
                    
                    $query_get_username = queryDB("SELECT name FROM users where name='$name'");
        
                    if ($query_get_username->num_rows == 0) {
                        return throwError("The username: $name not found in users database.");
                    } else {
                        $options = [
                            'cost' => 11
                        ];
                        $hash = password_hash($pass, PASSWORD_BCRYPT, $options);
                        
                        $query_update_password = queryDB("UPDATE users SET password = '$hash' where name='$name'");
                        
                        if ($mydb->affected_rows == 0){
                            return throwError("Password not updated for username='$name'");
                        } else {
                            $response = array("status"=>1,"desc"=>"Password updated for username='$name'");
                            echo json_encode($response);
                            return;
                        }
                    }
                    break;
                case 'create-user': // requires username, role, password
                    
                    $query_get_username = queryDB("SELECT name FROM users where name='$name'");

                    if ($query_get_username->num_rows == 0) { // no username exists, ur safe.
                        $options = [
                            'cost' => 11
                        ];
                        $hash = password_hash($pass, PASSWORD_BCRYPT, $options);

                        $query_create_user = queryDB("INSERT into users (name, role, password) VALUES ('$name', '$role', '$hash')");
                        if (!$query_create_user){
                            $k= mysqli_error($mydb);
                            return throwError("ERROR: $k");
                        }

                        if ($mydb->affected_rows == 0) { // not created
                            return throwError("Error: Could not add user='$name' despite not being in users DB.");
                        }else{ // added user
                            $response = array("status"=>1,"desc"=>"created user: '$name'");
                            echo json_encode($response);
                        }
                    } else {
                        return throwError("username='$name' already exists.");
                    }
                    break;
                case 'delete-user': // requires name. may add a non-admin call that requires pass.
                    
                    $query_username = queryDB("SELECT name FROM users where name='$name'");
        
                    if ($query_username->num_rows == 0) {
                        throwError("Username='$name' not found in users DB.");
                    } else {
                        $result = queryDB("DELETE from users where name='$name'");
        
                        if ($mydb->affected_rows == 0) {
                            return throwError("Username='$name' could not be deleted.");
                        } else {
                            $response = array("status"=>1,"desc"=>"deleted user: '$name'");
                            echo json_encode($response);
                        }
                    }
                    break;
                case 'send-data':
                    // just give a string, receive a string.

                    $sql_getUsername = "SELECT name FROM users where name='$name'";
                    $query_get_username = $mydb->query($sql_getUsername);

                    if ($query_get_username->num_rows == 0) { // name not recognized

                        $response = array("status"=>0,"desc"=>"could not locate username.");
                        echo json_encode($response);

                    } else { //name exists, lets add data.
                    
                        $sql_getUsername = "SELECT name FROM tester1 where name='$name'";
                        $query_get_username = $mydb->query($sql_getUsername);

                        if ($query_get_username->num_rows == 0) { // name not in table. CREATE it
                            $sql_create_user = "INSERT into tester1 (name, data) VALUES ('$name', '$sender_Data')";
                            $query_create_user = $mydb->query($sql_create_user);
                            if (!$query_create_user){
                                $k= mysqli_error($mydb);
                                return throwError("ERROR: $k");
                            }

                            if ($mydb->affected_rows != 0) { // created
                                $response = array("status"=>1,"desc"=>"created user: '$name'");
                                echo json_encode($response);
                            } else { // name in table, but couldnt create data for some reason.
                                $response = array("status"=>0,"desc"=>"could not create the username for some reason.");
                                echo json_encode($response);
                            }

                        } else { // name in table --> overwrite data
                            $sql_updateData = "UPDATE tester1 SET data = '$sender_Data' where name='$name'";
                            $query_data_updated = $mydb->query($sql_updateData);
            
                            $data_updated = $mydb->affected_rows != 0 ? 'true' : 'false';
            
                            $response = array("status"=>1,"desc"=>"data updated");
                            echo json_encode($response);
                        }
                    }
                    break;
                case 'get-data':
                    $sql_getUsername = "SELECT name FROM users where name='$name'";
                    $query_get_username = $mydb->query($sql_getUsername);
                
                    if ($query_get_username->num_rows == 0) { // name not recognized

                        $response = array("status"=>0,"desc"=>"could not locate usernames.");
                        echo json_encode($response);

                    } else { // name exists --> get data

                        $sql_getData = "SELECT data FROM tester1 where name='$name' ";
                        $query_getData = $mydb->query($sql_getData);
                
                        if ($query_getData->num_rows == 0) { // no data to receive?
                            $response = array("status"=>0,"desc"=>"unexpected error. data should have at least sent back null.");

                            echo json_encode($response);
                        } else {
                            $data = $query_getData->fetch_assoc()["data"];

                            $response = array("status"=>1,"data"=>$data);

                            $rr = json_encode($response);
                            echo $rr;
            
                            return;
                        }
                    }
                    break;
                case 'login-user':
                    $findme   = '=';
                    $pos = strpos($name, $findme);
                    if ($pos === false) {
                        // equals signs blocked.
                    } else {
                        // equals signs blocked.
                        throwError("?? Fuck off.");exit();
                    }


                    $query_get_user_data = queryDB("SELECT role , password FROM users where name='$name' ");
            
                    if ($query_get_user_data->num_rows == 0) { // no user found
                        return throwError("Username='$name' not found in users DB.");
                    } else {
                        $row = $query_get_user_data->fetch_assoc();

                        $try_pass = $row["password"];
                        $try_role = $row["role"];

                        if($debug_mode == 'true' || $debug_mode2 == 'true') {
                            echo "\nPassword testing:\nhash: $try_pass";
                            echo "\npass given: $pass \n";
                        }

                        // verify password using bcrypt decoder.
                        if(password_verify($pass, $try_pass)) {
                            $response = array("status"=>1,"desc"=>"Access Granted.", "role"=>$try_role );
                            echo json_encode($response);
                            return;
                        } else {
                            return throwError("Incorrect password given.");
                        }
                    }
                    break;
                case 'add-question':
                
                    // return throwError($cq_gradeable_list." ** ".$cq_gradeable_list);
                    $role_teacher = "teacher";

                    $verified = validate_user($mydb, $name, $role_teacher);
                    if(!$verified) return;
                    // okay, we're in. as a teacher
        
                    $sql_add_question = "INSERT into question_bank 
                        (description, name, gradable_list, function_name, difficulty, topic, question_constraint)
                        VALUES ('$cq_desc', '$cq_name', '$cq_gradeable_list', '$cq_func_name', '$cq_diff', '$cq_topic', '$cq_constraint')";
                    if (!$sql_add_question){
                        $k= mysqli_error($mydb);
                        return throwError("ERROR: $k");
                    }

                    $query_add_question = queryDB($sql_add_question);

                    if ($query_add_question===false){
                        throw new Exception($mydb->error);
                        return;
                    }

                    if ($mydb->affected_rows != 0) { // created

                        $query_getID = queryDB("SELECT id FROM question_bank ORDER BY id DESC LIMIT 1");
                        
                        $question_id = $query_getID->fetch_assoc()["id"];

                        $response = array("status"=>1,"id"=>$question_id);
                        echo json_encode($response);
                    } else { // error
                        throwError("Error: Failed to add question to question bank for some reason.");
                    }
                    break;
                case 'get-question':
                    
                    $verified = validate_user($mydb, $name);
                    if(!$verified) return;
                    // teacher AND student can retrieve question. don't add role blocker.

                    if($gq_id=="") { // if id is not specified
                        return throwError_IDNotFound("id");;
                    }

                    $query_getQuestion = queryDB("SELECT * FROM question_bank where id='$gq_id' ");
            
                    if ($query_getQuestion->num_rows == 0) { // item does not exist
                        throwError("question with id=$gq_id not found in the question bank.");
                    } else {
                        $row = $query_getQuestion->fetch_assoc();

                        $response = array(
                            "status"=>1,
                            "id"=>$row["id"],
                            "question_name"=>$row["name"],
                            "description"=>$row["description"],
                            "function_name"=>$row["function_name"],
                            "gradable_list"=>$row["gradable_list"],
                            "difficulty"=>$row["difficulty"],
                            "topic"=>$row["topic"],
                            "constraint"=>$row["question_constraint"]
                        );

                        echo json_encode($response);    
                        return;
                    }
                    break;
                case 'delete-question':
                    
                    $role_teacher = "teacher";

                    $verified = validate_user($mydb, $name, $role_teacher);
                    if(!$verified) return;
                    // am teacher

                    if($dq_id=="") { // should runu a validater for everything.
                        return throwError_IDNotFound("id");
                    }

                    $query_getID = queryDB("SELECT id FROM question_bank where id='$dq_id'");
        
                    if ($query_getID->num_rows == 0) {
                        return throwError("Question with id=$dq_id not found in question bank");
                    } else {
                        $rquery_delete_question = queryDB("DELETE from question_bank where id='$dq_id'");
        
                        if ($mydb->affected_rows == 0){ //question not deleted
                            return throwError("Error: Question with id=$dq_id could not be deleted.");
                        } else {
                            $response = array("status"=>1, "desc"=>"Question with id=$dq_id deleted.");
                            echo json_encode($response);
                            return;
                        }
                    }
                    break;
                case 'cue': // bleeds into the create-unposted-exam
                case 'create-unposted-exam': // requires: exam-name or name, 
                    
                    $role_teacher = "teacher";

                    $verified = validate_user($mydb, $name, $role_teacher);
                    if(!$verified) return;
                    // only instructor can create exams.

                    //test to make sure we are giving a correctly formatted array
                    $array_is_valid = validate_array_formatting($cue_question_point_pairs);
                    if(!$array_is_valid){
                        return throwError("Invalid format of question-point-pairs array.");
                    }
                    
                    $qpp_is_valid = validate_qp_pairs($cue_question_point_pairs);
                    if(!$qpp_is_valid){
                        return throwError("question-point-pairs array is incomplete or missing data.");
                    }

                    $spawned_exam_id = 0; // init it.

                    $query_create_exam = queryDB("INSERT into unposted_exams (name, question_point_pairs, spawned_exam_id) VALUES ('$cue_name','$cue_question_point_pairs', '$spawned_exam_id')");
                    if (!$query_create_exam){
                        $k= mysqli_error($mydb);
                        return throwError("ERROR: $k");
                    }

                    if ($mydb->affected_rows != 0) { // created
                        // get newest ID.
                        $query_getID = queryDB("SELECT id FROM unposted_exams ORDER BY id DESC LIMIT 1");

                        $id_unposted_local = $query_getID->fetch_assoc()["id"];

                        $type_unposted = "unposted";
                        $query_add_exam = queryDB("INSERT into exam_list (ref_id, name, type) VALUES ('$id_unposted_local','$cue_name','$type_unposted')");
                        if (!$query_add_exam){
                            $k= mysqli_error($mydb);
                            return throwError("ERROR: $k");
                        }

                        $query_get_examList_ID = queryDB("SELECT id FROM exam_list ORDER BY id DESC LIMIT 1");
                        $id_unposted_exam_global = $query_get_examList_ID->fetch_assoc()["id"];

                        if ($mydb->affected_rows == 0) { // exam not added to exam list. needs to remove the last exam.
                            
                            // attempt to delete newly created unposted exam.
                            $query_delete_unposted_exam = queryDB("DELETE from unposted_exams where id='$id_unposted_local'");

                            $error_msg_prefix = "Error: Exam '$cue_name' added to unposted_exams DB but could not add to exam list.";

                            if ($mydb->affected_rows != 0) { // unposted exam successfully deleted.
                                throwError($error_msg_prefix . " Exam sucessfully deleted from unposted exams list.");
                            } else { // unposted exam not deleted. throw error.
                                throwError($error_msg_prefix . "\nError: Exam could not be deleted from unposted exams list.");
                            }
                            return;
                        } else { // exam added to unposted exams list and exams list.
                            
                            $response = array(
                                "status"=>1,
                                "desc"=>"Exam (unposted) created. Name='$cue_name, global id=$id_unposted_exam_global, local id=$id_unposted_local",
                                "global id"=>$id_unposted_exam_global,
                                "local id"=>$id_unposted_local
                            );
                            echo json_encode($response);
                            return;
                        }
                    } else {
                        return throwError("Error: Could not add exam to unposted exams list.");
                    }
                    break;
                case 'pue':
                case 'post-ungraded-exam': // unposted exams can only be seen by teachers. once they post, anyone can edit and repost.
                    
                    $verified = validate_user($mydb, $name);
                    if(!$verified) return;
                    
                    $query_getExam = queryDB("SELECT ref_id, type FROM exam_list where id='$pue_id'");

                    if($pue_id==""){//check if id exists.
                        return throwError_IDNotFound("global-id");
                    } else if ($query_getExam->num_rows == 0) {
                        return throwError_examNotFound($pue_id);
                    }

                    $row = $query_getExam->fetch_assoc();
                    $local_id = $row["ref_id"];
                    $exam_type = $row["type"];
                    
                    switch($exam_type){
                        case 'unposted':                        
                            
                            $query_getQuestionList = queryDB("SELECT question_point_pairs FROM unposted_exams WHERE id='$local_id'");
                            
                            if ($query_getQuestionList->num_rows == 0) { // nothing found.
                                return throwError("Error: Could not retrieve question-point-pairs from exam id=$local_id from unposted exams list.");
                            }

                            $row = $query_getQuestionList->fetch_assoc();
                            $qpp_dict = $row["question_point_pairs"];
                            $qp_pairs = json_decode($qpp_dict);
                            
                            $global_comment = $pue_global_comment=="" ? "No comment." : $pue_global_comment;
                            $type_ungraded = "ungraded";
                            $is_submitted = "false";
                            $query_addValues = queryDB(
                                "INSERT INTO posted_exams (exam_table_name, exam_name, type, global_comment, is_submitted) 
                                VALUES (' ', '$pue_name', '$type_ungraded', '$global_comment', '$is_submitted') "
                            );
                            if (!$query_addValues){
                                $k= mysqli_error($mydb);
                                return throwError("ERROR: $k");
                            }

                            if ($mydb->affected_rows == 0) {
                                return throwError("Could not add exam with name='$pue_name' to posted exams list.");
                            } else {
                                // get newly created local id for posted exam
                                $query_get_exam_ID = get_newest_value('exam_id', 'posted_exams');
                                
                                $id_posted_ungraded_local = $query_get_exam_ID->fetch_assoc()["exam_id"];

                                // add posted exam to exams list
                                $query_add_exam = queryDB(
                                    "INSERT into exam_list (ref_id, name, type) 
                                    VALUES ('$id_posted_ungraded_local','$pue_name','$type_ungraded')"
                                    );
                                if (!$query_add_exam){
                                    $k= mysqli_error($mydb);
                                    return throwError("ERROR: $k");
                                }

                                // get newly created global id for posted exam
                                $query_get_examList_ID = get_newest_value('id', 'exam_list');
                                $id_posted_ungraded_global = $query_get_examList_ID->fetch_assoc()["id"];
            
                                if ($mydb->affected_rows == 0) { // exam not added to exam list. remove the posted exam.
            
                                    // attempt to delete newly created posted exam.
                                    $query_delete_posted_exam = queryDB("DELETE from posted_exams where id='$id_posted_ungraded_local'");

                                    $error_msg_prefix = "Error: Exam '$pue_name' added to posted_exams DB but could not add to exam list.";

                                    if ($mydb->affected_rows != 0) { // posted exam successfully deleted.
                                        throwError($error_msg_prefix . " Exam sucessfully deleted from posted exams list.");
                                    } else {
                                        throwError($error_msg_prefix . "\nError: Exam could not be deleted from posted exams list.");
                                    }
                                    return;
                                } else { // added ungraded to exam_list. now generate the table
                                    /*  
                                                    reminder: THIS IS THE POST *UNGRADED* EXAM FUNCTION. 
                                                        PUE             PUE             PUE
                                    */

                                    $pe_tableName = "peTable_$id_posted_ungraded_global"; // generate a table name.

                                    $query_set_exam_table_name = queryDB("UPDATE posted_exams SET exam_table_name='$pe_tableName' WHERE exam_id='$id_posted_ungraded_local'");
                                    $query_set_exam_spawned_id = queryDB("UPDATE unposted_exams SET spawned_exam_id='$id_posted_ungraded_global' WHERE id='$local_id'");

                                    //generate exam table
                                    $exam_is_generated = generate_posted_exam($pe_tableName, $pue_name, $id_posted_ungraded_global, $id_posted_ungraded_local);
                                    if (!$exam_is_generated){
                                        return throwError("Failed to generate exam.");
                                    }

                                    foreach($qp_pairs as $qp_pair){
                                        $question_id = -1; $points = -1;
                                        foreach ($qp_pair as $k=>$v){
                                            $q_or_p = parse_qp_pair($k, $v);
                                            if (!$q_or_p){
                                                return throwError("ERROR: Parsing error in 'pue'->parse_qp_pair().");
                                            } else {
                                                if ($q_or_p['type'] == 'q'){
                                                    $question_id = $q_or_p['value'];
                                                } else {
                                                    $points = $q_or_p['value'];
                                                }
                                            }
                                        }

                                        $response_data = " ";
                                        $local_comment = " ";
                                        $grade_array = array(
                                            'given'=>'',
                                            'expected'=>'',
                                            'auto'=>'',
                                            'manual'=>'',
                                            'comment'=>''
                                        );
                                        $grade_tuple = json_encode($grade_array);

                                        $point_score_array = array(
                                            "max_points"=>$points,
                                            "score"=>0
                                        );
                                        $point_score_pair = json_encode($point_score_array);

                                        $query_add_exam_data = queryDB(
                                            "INSERT into $pe_tableName (question_id, response_data, point_score_pair, question_comment, grade_tuple) 
                                            VALUES ('$question_id', '$response_data', '$point_score_pair', '$local_comment', '$grade_tuple')"
                                        );
                                        if (!$query_add_exam_data){
                                            $k= mysqli_error($mydb);
                                            return throwError("ERROR: $k");
                                        }

                                        if ($mydb->affected_rows == 0) {
                                            return throwError("Could not add exam with name='$pue_name' to posted exams list.");
                                        }
                                    }

                                    $response = array("status"=>1,
                                        "desc"=>"Ungraded exam added. Name='$pue_name' | global id=$id_posted_ungraded_global | local id=$id_posted_ungraded_local",
                                        "global id"=>$id_posted_ungraded_global,
                                        "local id"=>$id_posted_ungraded_local
                                    );
                                    echo json_encode($response);
                                    return;
                                }
                            }

                            break;
                        case 'ungraded': // 
                            return throwError("Attempted to post an already ungraded exam with mode='post-ungraded-exam' or 'pue'.
                                                \nUse 'modify-ungraded-exam' or 'mue' instead.");

                        case 'graded': // make calls explicit and directed
                            return throwError("Attempted to post a graded exam with mode='post-ungraded-exam' or 'pue'.
                                                \nUse 'regrade-exam' instead.");
                        default:
                            return throwError("Error: Invalid exam type: type=$exam_type");
                    }
                    break;
                case 'mue':
                case 'modify-ungraded-exam':


                    // this is an overwrite call. 
                    // it will not do validation on whether you returned the correct questions or the correct order of questions.

                    // it's just an overwrite call. it will drop table. confirmed. this does not increase any auto-inc ints.

                    $verified = validate_user($mydb, $name);
                    if(!$verified) return;

                    // pull exam from exam_list with the global_id. get back a local id
                    $query_getExam = queryDB("SELECT ref_id, type FROM exam_list where id='$mue_id'");

                    // if no id given. if global id not found
                    if ($pge_id == ""){
                        return throwError_IDNotFound("global-id");
                    } else if ($query_getExam->num_rows == 0) {
                        return throwError_examNotFound($pge_id);                    
                    }

                    $row = $query_getExam->fetch_assoc();
                    $local_id = $row["ref_id"];
                    $exam_type = $row["type"];

                    switch($exam_type){ //ignore all but ungraded.
                        case 'unposted': return throwError("Attempted to grade an unposted exam. id=$pge_id. \nPost it first using
                            'mode'='post-ungraded-exam' or 'pue'");
                        case 'ungraded':
                            // constantly needing stuff from posted exam.
                            $query_get_ungraded_data = queryDB("SELECT * FROM posted_exams WHERE exam_id='$local_id'");
                            if ($query_get_ungraded_data->num_rows == 0) { // nothing found.
                                return throwError("Error: ungraded exam with id=$local_id not found in posted exams list.");
                            }
                            $exam_data = $query_get_ungraded_data->fetch_assoc();
                            $original_exam_name = $exam_data['exam_name'];
                            $exam_table_name = $exam_data['exam_table_name'];
                            $original_global_comment = $exam_data['global_comment'];

                            $exam_name_changed = 0; $exam_global_comment_changed = 0;
                            if ($mue_name != "") {
                                //you need to update the names of exam list and 
                                // exam list name is 'name' -- posted exams list name is exam_name
                                $query_set_new_exam_name_global = queryDB(
                                    "UPDATE exam_list SET name='$mue_name' WHERE id='$mue_id'"
                                );
                                $query_set_new_exam_name_local = queryDB(
                                    "UPDATE posted_exams SET exam_name='$mue_name' WHERE exam_id='$local_id'"
                                );

                                $exam_name_changed = 1;
                                // @TODO: call errors if these don't work. add later.
                                //echo "exam name changed.\n";
                            }

                            // update global comment.
                            if ($mue_global_comment != "") {
                                $query_set_new_global_comment = queryDB(
                                    "UPDATE posted_exams SET global_comment='$mue_global_comment' WHERE exam_id='$local_id'"
                                );

                                $exam_global_comment_changed = 1;
                                // @TODO: call errors if these don't work. add later.
                                //echo "exam global comment changed.\n";
                            }

                            /*  
                                        reminder: THIS IS THE MODIFY *UNGRADED* EXAM FUNCTION. 
                                                MUE             MUE             MUE
                            */
                            
                            if ($mue_data == "") { // if no data given, then end here and check for any updates.
                                
                                $name_msg = $exam_name_changed ? "New name='$mue_name'" : "Name=$original_exam_name";
                                $comment_msg = $exam_global_comment_changed ? "New global comment='$mue_global_comment'" : "global comment=$original_global_comment";

                                if($exam_name_changed || $exam_global_comment_changed){
                                    $response = array("status"=>1,
                                        "desc"=>"Ungraded exam modified. $name_msg, $comment_msg, global id=$mue_id, local id=$local_id.",
                                        "exam name"=> $exam_name_changed ? $mue_name : $original_exam_name,
                                        "global comment"=> $exam_global_comment_changed ? $mue_global_comment : $original_global_comment,
                                        "global id"=>$mue_id,
                                        "local id"=>$local_id
                                    );
                                    echo json_encode($response);
                                } else {
                                    throwError("No data given. Exam not updated.");
                                }
                                return;
                            }
                            // --------- data given, proccess data below. --------------

                            // you need to decode data. it is sent as a string
                            $mue_array = json_decode($mue_data);

                            // let's start off by dissecting data given.
                            // foreach ($mue_array as $qblock) {
                            //     foreach ($qblock as $what) {
                            //         // return throwError("hi: ". $what);//what without a call is id.
                            //         // return throwError("hello: ". $qblock->id);//$qblock->id is valid.
                            //         return throwError("YES: ". $qblock->response);//$qblock->response is valid.
                            //         // return throwError("maybe: ". $qblock['id']);//question called like a dictionary does not work.
                            //     }
                                
                            // }

                            // do i really need point/score pairs? that's literally saved in the question-space.
                            // when you click 'grade exam', it will go thru each question:response pair, 
                            //
                            //              !! IN ** GRADE EXAM **
                            //
                            //      use question values to contrast against response data.
                            //      use response to build a caller.
                            //          run foreach tcsp(i) == sol(i) ? new points+=val
                            //          run functionname == parsed function name ? new points+=val
                            //          run constraint found in response ? new points+=val
                            //
                            //      for each thing ^ above, u get 
                            //
                            //          given           <-- extracted.
                            //          auto-grade      <-- spawned
                            //          manual-grade='' <-- empty init
                            //          expected        <-- pulled from >question< of the question:response pair
                            //          comment         <-- empty init
                            //          
                            //          **these values are all generated after you autograde the exam** apart of grade-exam.
                            //
                            //           

                            //okay. so i got the basics from this. kinda fucking stupid, but w/e. moving on. 
                            // i just need an id and response. what do i need to do to clear everything else?

                            $validate_mode_on = 1;
                            $array_is_valid = validate_mue_data_array($mue_array, $local_id, $validate_mode_on);
                            if (!$array_is_valid){
                                return throwError("ERROR: Invalid array formay provided to modify-ungraded-exam function.");
                            }


                            try {
                                $query_drop_postedExam_table = queryDB("DROP TABLE $exam_table_name");
                            } catch (PDOException $e) {
                                return throwError("ERROR: $e");
                            }

                            //generate exam table
                            $exam_is_generated = generate_posted_exam($exam_table_name, $mue_name, $mue_id, $local_id);
                            if (!$exam_is_generated){
                                return throwError("Failed to generate exam.");
                            }

                            // getting here means you are safe don't error handle. populate the table with the new data.
                            repopulate_ungraded_exam($mue_array, $local_id); // this is purely for repopulation. nothing else. stop mentioning it.

                            if($mue_exam_isSubmitted=='true'){
                                queryDB("UPDATE posted_exams SET is_submitted='$mue_exam_isSubmitted' WHERE exam_id='$local_id'");
                            }

                            $name_msg = $exam_name_changed ? "New name='$mue_name'" : "Name=$original_exam_name";
                            $comment_msg = $exam_global_comment_changed ? "New global comment='$mue_global_comment'" : "global comment=$original_global_comment";
                            
                            $response = array("status"=>1,
                                "desc"=>"Ungraded exam modified. $name_msg, $comment_msg, global id=$mue_id, local id=$local_id.",
                                "exam name"=> $exam_name_changed ? $mue_name : $original_exam_name,
                                "global comment"=> $exam_global_comment_changed ? $mue_global_comment : $original_global_comment,
                                "global id"=>$mue_id,
                                "local id"=>$local_id
                            );
                            echo json_encode($response);
                            return;
                        case 'graded':
                            return throwError("Attempted to post an already graded exam with mode='post-graded-exam' or 'pge'.
                                                \nUse 'regrade-exam' instead.");
                        default:
                            return throwError("Error: Invalid exam type: type=$exam_type");
                    }
                    break;
                case 'pge':
                case 'grade-exam':
                case 'post-graded-exam':
                    // just wants the global id of an ungraded exam, and a name. must be a teacher.
                    
                    $role_teacher = "teacher";

                    $verified = validate_user($mydb, $name, $role_teacher);
                    if(!$verified) return;

                    // pull exam from exam_list with the global_id. get back a local id
                    $query_getExam = queryDB("SELECT ref_id, type, name FROM exam_list where id='$pge_id'");

                    // if no id given. if not id found
                    if ($pge_id == ""){
                        return throwError_IDNotFound("global-id");
                    } else if ($query_getExam->num_rows == 0) {
                        return throwError_examNotFound($pge_id);                    
                    }

                    $row = $query_getExam->fetch_assoc();
                    $local_id = $row["ref_id"];
                    $exam_type = $row["type"];
                    $exam_name = $row["name"];

                    switch($exam_type){
                        case 'unposted': return throwError("Attempted to grade an unposted exam. id=$pge_id. \nPost it first using
                                                    'mode'='post-ungraded-exam' or 'pue'");
                        case 'ungraded':

                            $pge_grade_tuple_str =  $pge_grade_tuple;
                            $pge_grade_tuple_obj = json_decode($pge_grade_tuple_str);

                            $temp = array();
                            foreach($pge_grade_tuple_obj as $k=>$v){
                                $temp[$k]= json_encode($v);
                            }
                            
                            $query_get_ungraded_exam_table_name = queryDB("SELECT exam_table_name, global_comment FROM posted_exams WHERE exam_id='$local_id'");

                            // if the posted exam doesn't exist.
                            if ($query_get_ungraded_exam_table_name->num_rows == 0) {
                                return throwError("Error: Could not find exam with exam_id=$local_id in posted exams list.");
                            }

                            $result = $query_get_ungraded_exam_table_name->fetch_assoc();
                            $exam_table_name = $result["exam_table_name"];
                            $global_comment = $result["global_comment"];
                            

                            $type_graded = "graded";
                            $is_submitted = "true";
                            $pge_name = $pge_name == '' ? $exam_name : $pge_name;

//# kill this insert. ur not adding anything to posted.
                            $query_set_exam_table_name = queryDB("UPDATE posted_exams SET type='$type_graded' WHERE exam_id='$local_id'");
                            if ($mydb->affected_rows == 0) {
                                return throwError("Error: Could not update exam_table_name in posted exams list.");
                            }

                            $query_set_exam_table_name = queryDB("UPDATE exam_list SET type='$type_graded' WHERE id='$pge_id'");
                            if ($mydb->affected_rows == 0) {
                                return throwError("Error: Could not update exam_table_name in posted exams list.");
                            }

                            // exam added to posted exams list and global exams list
                            /*  
                                    reminder: THIS IS THE POST *GRADED* EXAM FUNCTION. 
                                        PGE             PGE             PGE
                            */

                            $graded_exam_tableName = "peTable_$pge_id"; // generate a table name.

//# finally here it is!!!

                            // get ROWS of data from generated exam with name=$exam_table_name. 
                            // -- yeah i need this to copy over vals that should not be changed.
                            $query_get_exam_data = queryDB("SELECT * FROM $exam_table_name");

                            if ($query_get_exam_data->num_rows == 0) {
                                return throwError("Error: No data found in table: '$exam_table_name'.");
                            }

                            // this is literally just going to duplicate pue. that's it. stop asking about this. it's a dumb method.

                            while($row = $query_get_exam_data->fetch_assoc()) {
                                $question_id = $row["question_id"];

                                $query_delete_exam = queryDB("DELETE from $graded_exam_tableName where question_id='$question_id'");
                                if (!$query_delete_exam){
                                    $k= mysqli_error($mydb);
                                    return throwError("ERROR: $k");
                                }

                                $response_data = $row["response_data"];
                                $point_score_pair = $row["point_score_pair"];
                                $question_comment = $row["question_comment"];
                                $grade_tuple= $temp[$question_id];

                                $query_add_exam_data = queryDB(
                                    "INSERT into $graded_exam_tableName (question_id, response_data, point_score_pair, question_comment, grade_tuple) 
                                        VALUES ('$question_id', '$response_data','$point_score_pair', '$question_comment', '$grade_tuple')"
                                    );
                                if (!$query_add_exam_data){
                                    $k= mysqli_error($mydb);
                                    return throwError("ERROR: $k");
                                }
                            }
                            
                            $response = array("status"=>1,
                                "desc"=>"Graded exam added. Name='$pge_name', global id=$pge_id, local id=$local_id.",
                                "global id"=>$pge_id,
                                "local id"=>$local_id
                            );
                            echo json_encode($response);
                            return;
                            
                            
                            break;
                        case 'graded':
                            return throwError("Attempted to post an already graded exam with mode='post-graded-exam' or 'pge'.
                                                \nUse 'regrade-exam' instead.");
                        default:
                            return throwError("Error: Invalid exam type: type=$exam_type");
                    }
                    break;
                case 'get-exam':
                    
                    $query_getExam_vals = queryDB("SELECT * FROM exam_list where id='$ge_id'");

                    $row = $query_getExam_vals->fetch_assoc();
                    $local_id = $row["ref_id"];
                    $exam_type = $row["type"];
            
                    if ($query_getExam_vals->num_rows == 0) { // exam does not exist
                        return throwError_examNotFound($ge_id);
                    } else {
                        switch($exam_type){
                            case 'unposted':

                                $role_teacher = "teacher";
                                $verified = validate_user($mydb, $name, $role_teacher);
                                if(!$verified){
                                    return throwError("Access Denied. Only teachers can retrieve unpotsed exams.");
                                }
                                // only teachers can retrieve at unposted exams

                                $query_get_unposted_exam = queryDB("SELECT * FROM unposted_exams where id='$local_id'");

                                if ($query_get_unposted_exam->num_rows == 0) { // item does not exist
                                    return throwError("Error: exam with id=$local_id not found in unposted exams list.");
                                } else {
                                    $row = $query_get_unposted_exam->fetch_assoc();
                
                                    $response = array(
                                        "status"=>1,
                                        "global id"=>$de_id,
                                        "local id"=>$row["id"],
                                        "exam name"=>$row["name"],
                                        "question-point-pairs"=>$row["question_point_pairs"],
                                        "spawned-exam-id"=>$row["spawned_exam_id"]
                                    );
                                    if ($internal_call==true) {
                                        $internal_call=false;
                                        return $response;
                                    }
                                    echo json_encode($response);
                                    return;
                                }
                                break;
                            case 'ungraded':
                            case 'graded':
                                $verified = validate_user($mydb, $name);
                                if(!$verified) return;

                                $query_get_ungraded_exam = queryDB("SELECT * FROM posted_exams where exam_id='$local_id' and type='$exam_type'");

                                if ($query_get_ungraded_exam->num_rows == 0) { // no exams found
                                    throwError("Error: exam with id=$local_id not found in posted exams list.");
                                } else {
                                    // extract exam info
                                    $exam_info_posted = $query_get_ungraded_exam->fetch_assoc();
                                    
                                    $pe_tableName = $exam_info_posted["exam_table_name"];
                                    $exam_name = $exam_info_posted["exam_name"];
                                    $is_submitted = $exam_info_posted["is_submitted"];
                                    $global_comment = $exam_info_posted["global_comment"];//global comment

                                    $query_get_posted_exam_data = queryDB("SELECT * FROM $pe_tableName");
                
                                    $exam_data = array();

                                    if ($query_get_posted_exam_data->num_rows == 0)  {
                                        // this isn't necessarily an error.
                                        return throwError("Error: No data found in posted_exam with global id=$ge_id.");
                                    }
                                    else {
                                        while($row = $query_get_posted_exam_data->fetch_assoc()) {
                                            // return an array of rows
                                            $arr = array(
                                                "question id"=>$row["question_id"],
                                                "point score pair"=> $row["point_score_pair"],
                                                "response data"=>$row["response_data"],
                                                "local comment"=>$row["question_comment"],
                                                "grade_tuple"=>$row["grade_tuple"]
                                            );
                                            array_push($exam_data, $arr);
                                        }
                    
                                        $response = array(
                                            "status"=>1,
                                            "exam name"=>$exam_name,
                                            "type"=>$exam_type,
                                            "global id"=>$ge_id,
                                            "local id"=>$local_id,
                                            "exam data"=>$exam_data,
                                            "is_submitted"=>$is_submitted,
                                            "global comment"=>$global_comment
                                        );

                                        if ($internal_call==true) {
                                            $internal_call=false;
                                            return $response;
                                        }
                                        echo json_encode($response);
                                        return;
                                    }
                                }
                                break;
                            default:
                                return throwError("Error: Invalid exam type: type=$exam_type");
                        }
                    }
                    break;
                case 'delete-exam':
                    //problem. u need to expand out ur spawned exam value.

                    $role_teacher = "teacher";

                    $verified = validate_user($mydb, $name, $role_teacher);
                    if(!$verified) return;
                    // teachers only.

                    if($de_id=="") {
                        return throwError_IDNotFound("id");
                    }
                    $query_getID = queryDB("SELECT ref_id, type, name FROM exam_list where id='$de_id'");

                    $row = $query_getID->fetch_assoc();

                    $local_id = $row["ref_id"];
                    $exam_type = $row["type"];
                    $exam_name = $row["name"];
        
                    if ($query_getID->num_rows == 0) {
                        return throwError_examNotFound($de_id);
                    } else {

                        $sql_delete_exam = "DELETE from exam_list where id='$de_id'";
                        $query_delete_exam = $mydb->query($sql_delete_exam);
        
                        if ($mydb->affected_rows == 0) { // not deleted
                            return throwError("Could not delete exam with global_id=$de_id from exam list.");
                        } else {
                            switch($exam_type){
                                case 'unposted':
                                    $query_delete_unposted_exam = queryDB("DELETE from unposted_exams where id='$local_id'");

                                    if ($mydb->affected_rows == 0) {
                                        return throwError("ERROR: exam id=$de_id removed from exam list, but not removed from unposted exams list.");
                                    }else{
                                        $response = array("status"=>1,"desc"=>"Exam deleted. Name='$exam_name', global id=$de_id, local id=$local_id.");
                                        echo json_encode($response);
                                    }
                                    break;
                                case 'ungraded':
                                case 'graded':
                                    return delete_posted_exam($de_id, $local_id);
                                default:
                                    return throwError("Error: Invalid exam type: type=$exam_type");
                            }
                        }
                        return;
                    }
                    break;
                case 'list-exams':
                    //just return all of the exams as an array.
                    $sql_getExam = "SELECT * FROM exam_list";
                    $query_getExam = $mydb->query($sql_getExam);

                    $ret = array();

                    if ($query_getExam->num_rows > 0) {
                        while($row = $query_getExam->fetch_assoc()) {
                            $arr = array(
                                "name"=>$row["name"],
                                "global id"=>$row["id"],
                                "local id"=>$row["ref_id"],
                                "type"=>$row["type"]
                            );
                            array_push($ret, $arr);
                        }

                        $response = array(
                            "status"=>1,
                            "exams"=>$ret
                        );
                        if ($internal_call==true) {
                            $internal_call=false;
                            return $response;
                        }
                        echo json_encode($response);
                        return;
                    } else {
                        return $internal_call == true ? 0 : throwError("No exams found.");
                    }
                    break;
                case 'list-questions':

                    $sql_getExam = "SELECT * FROM question_bank";
                    $query_getExam = $mydb->query($sql_getExam);

                    $ret = array();

                    if ($query_getExam->num_rows > 0) {
                        while($row = $query_getExam->fetch_assoc()) {
                            $arr = array(
                                "name"=>$row["name"],
                                "id"=>$row["id"]
                            );
                            array_push($ret, $arr);
                        }

                        $response = array(
                            "status"=>1,
                            "questions"=>$ret
                        );
                        echo json_encode($response);

                        return;
                    } else {
                        return throwError("No exams found.");
                    }
                    break;
                
                case 'filter':
                case 'filter-questions':

                    $fa = array("easy"=>1, "medium"=>2, "hard"=>3,
                            "1"=>1, "2"=>2, "3"=>3
                    );
                    $fd = $fa[$filter_diff];

                    if ($filter_topic == '' && $fd==''){
                        $query_getQuestions = queryDB("SELECT * FROM question_bank");
                    }
                    else if($filter_topic==''){
                        $query_getQuestions = queryDB("SELECT * FROM question_bank where difficulty='$fd'");
                    }
                    else if($fd==''){
                        $query_getQuestions = queryDB("SELECT * FROM question_bank where topic='$filter_topic'");
                    } else {
                        $query_getQuestions = queryDB("SELECT * FROM question_bank where topic='$filter_topic' and difficulty='$fd'");
                    }

                    
                    if ($query_getQuestions->num_rows > 0) {
                        
                        $keyword_found = false;
    
                        $matched_questions = array();

                        while($question = $query_getQuestions->fetch_assoc()) {
                            $question_id = $question['id'];
                            $matched_question = search_question_for_keywords($question_id, $filter_keyword, $filter_keyword_delimiter);
                            if($matched_question > 0) {
                                
                                //question found. add ret to list.
                                array_push($matched_questions, $matched_question);
                                $keyword_found = true;
                            } else if ($matched_question==-1){
                                return throwError("Error: question with id $question_id somehow not found.");
                            } else if ($matched_question==0){
                                // does nothing, but im leaving this call here to explain that the function only returns 0, -1, or a value.
                            }
                        }
    
                        if ($keyword_found){
                            $response = array("status"=>1,
                                "filtered_questions"=>$matched_questions,
                                'delete-this-call'=> $filter_keyword
                                );
                                echo json_encode($response);
                        } else {
                            return throwError("keyword: '$filter_keyword' not found in question bank.");
                        }
                    } else {
                        return throwError("Questions with topic='$filter_topic' and difficulty='$fd' not found.");
                    }
                    break;
                case '--modes':
                    echo "Modes ~Usage: \"mode=<mode_name>\"~:\nlogin-user :: create-user :: delete-user :: send-data :: get-data :: set-pass ";
                    break;
                
                default: //test api here too. mode not needed.
                    echo "\n\tmode not provided. \n\tUse \"mode = --m\" to view available modes.";
            }
    }



        function validate_user($db, $name, $role = -1){

            $sql_getUsername = "SELECT name, role FROM users where name='$name'";
            $query_get_username = $db->query($sql_getUsername);

            if ($query_get_username->num_rows == 0) { // name not recognized

                $response = array("status"=>0,"desc"=>"could not locate username '$name'.");
                echo json_encode($response);
                return false;
            }

            if($role != -1) {
                $row = $query_get_username->fetch_assoc();
                if($row["role"] != $role) {
                    throwError("Permission Denied");
                    return false;
                }
            }
            
            return true;
        }

        function throwError($msg){
            $response = array("status"=>0,"desc"=>$msg);
            echo json_encode($response);
        }

        function throwError_examNotFound($exam_id){
            return throwError("Exam with 'global-id'=$exam_id not found in exam list.");
        }

        function throwError_IDNotFound($expected_id_post_type){
            return throwError("Error: No ID given. Expected '$expected_id_post_type'.");
        }

        function queryDB($query_string){
            global $mydb;
            return $mydb->query($query_string);
        }

        function delete_posted_exam($global_id, $local_id){
            global $mydb, $de_spawner_exam;

            $query_getID = NULL;
            try{
                $query_getID = queryDB("SELECT exam_table_name, exam_name FROM posted_exams where exam_id='$local_id'");
            } catch (PDOException $e){
                throwError("ERROR: $e");
                exit();
            }

            $result = $query_getID->fetch_assoc();
            $pe_tableName = $result["exam_table_name"];
            $exam_name = $result["exam_name"];

            $sql_delete_posted_exam = NULL;
            try{
                $sql_delete_posted_exam = "DELETE from posted_exams where exam_id='$local_id'";
            } catch (PDOException $e){
                throwError("ERROR: $e");
                exit();
            }
            $query_delete_posted_exam = $mydb->query($sql_delete_posted_exam);

            if ($mydb->affected_rows == 0) {
                throwError("ERROR: exam id=$global_id removed from exam list, but not removed from posted exams list.");
                exit();
            } else{
                $query_drop_postedExam_table = queryDB("DROP TABLE $pe_tableName");
                if($de_spawner_exam!=''){
                    $query_get_spawner = queryDB("SELECT ref_id FROM exam_list where id='$de_spawner_exam'");
                    $spawner_id = $query_get_spawner->fetch_assoc()['ref_id'];
                    $spawned_exam_id = 0;

                    // u need to do: in unposted_exam list, go to exam that pressed it. look a
                    $query_update_pue_spawned = queryDB("UPDATE unposted_exams SET spawned_exam_id='$spawned_exam_id' WHERE id='$spawner_id'");
                }

                $response = array("status"=>1,"desc"=>"Exam '$exam_name' deleted. (global id=$global_id :: local id=$local_id).");
                echo json_encode($response);
            }
        }

        function get_newest_value($target_value, $from_table){
            return queryDB("SELECT $target_value FROM $from_table ORDER BY $target_value DESC LIMIT 1");
        }
        
        function generate_posted_exam($exam_table_name, $exam_name, $id_exam_global, $id_exam_local){
            global $mydb;
            $sql_createTable = "CREATE TABLE $exam_table_name (
                id INT(8) AUTO_INCREMENT PRIMARY KEY,
                question_id INT(32),
                response_data VARCHAR(512),
                point_score_pair VARCHAR(64),
                question_comment VARCHAR(512),
                grade_tuple VARCHAR(1024)
                )";
            
            if ($mydb->query($sql_createTable) !== TRUE) { // could not create the exam
                throwError("Error: Could not create <<generate>> posted exam table name='$exam_name'.");
                delete_posted_exam($id_exam_global, $id_exam_local);
                return 0;
            }
            return 1;
        }

        function validate_array_formatting($string_array_to_validate){
            
            $decoded_form=json_decode($string_array_to_validate);
            $type=gettype($decoded_form);
            return $type == "array" ? 1 : 0;
        }

        function validate_qp_pairs($qp_pairs_array_as_string){
            $alias_qs = array('q', 'id', 'question_id');
            $alias_ps = array('p', 'pts', 'points');
            
            // $k = key, $v = value
            $qp_pairs_as_array = json_decode($qp_pairs_array_as_string);

            foreach($qp_pairs_as_array as $pair){ //runs 4 times
                $qid = -1; $pts = -1; 
                foreach($pair as $k=>$v){ //for each key/value pair.
                    if (in_array($k, $alias_qs) || in_array($k, $alias_ps)){ //ok. it's allowed
                        if (in_array($k, $alias_qs)){
                            if ($qid != -1) 
                            {
                                throwError("Duplicate labels/keys found in qpp.");
                                return 0;
                            }
                            $qid = $v;
                        } else {
                            if ($pts != -1) {
                                throwError("Duplicate labels/keys found in qpp.");
                                return 0;
                            }
                            $pts = $v;
                        }
                    } else {
                        throwError("Incorrect format of question_point pair. Expected points=>x,.");
                        return 0;
                    }
                }
                if( $qid == -1 || $pts == -1) { // optional: if you are missing any.
                    throwError("Incorrect format of question_point pair. Expected points=>x,.");
                    return 0;
                }

                // echo " question id: $qid | points: $pts \n";
                // echo " =============== \n";
            }
            return 1;
        }

        function parse_qp_pair($k, $v){ // this can only give you 1 item. can't give both.
            $alias_qs = array('q', 'id', 'question_id');
            $alias_ps = array('p', 'pts', 'points');

            $type = -1; $val = -1;

            if (in_array($k, $alias_qs) || in_array($k, $alias_ps)){ //ok. it's allowed
                if (in_array($k, $alias_qs)){
                    $val = $v;
                    $type = 'q';
                } else {
                    $val = $v;
                    $type = 'p';
                }
            } else {
                throwError("Incorrect format of question_point pair. Unrecognized label given: $k");
                return 0;
            }

            return array("type"=>$type, "value"=>$val);
        }

        function parse_exam_question_data($k, $v){ // this can only give you 1 item. can't give both.
            
            $alias_qs = array('q', 'id', 'question_id', 'question id');     // question id
            $alias_ps = array('p', 'pts', 'points', 'max_points');          // points
            $alias_score = array('s', 'score');                             // score
            $alias_data = array('d', 'response', 'data', 'response_data', 'response data'); // response
            $alias_comment = array('c', 'comment', 'local comment');        // question comment

            $type = -1; $val = -1;

            if ( // it's allowed
                in_array($k, $alias_qs) || in_array($k, $alias_ps) 
                || in_array($k, $alias_score) || in_array($k, $alias_data)
                || in_array($k, $alias_comment)
            ){ 
                if (in_array($k, $alias_qs)){
                    $type = 'q';
                } else if (in_array($k, $alias_ps)) {
                    $type = 'p';
                } else if (in_array($k, $alias_score)) {
                    $type = 's';
                } else if (in_array($k, $alias_data)) {
                    $type = 'd';
                } else if (in_array($k, $alias_comment)) {
                    $type = 'c';
                } // no else because I want to group these explicitly.
                $val = $v;
            } else {
                throwError("Incorrect format for exam data. Unrecognized label given: $k");
                exit();
            }

            return array("type"=>$type, "value"=>$val);
        }

        function validate_mue_data_array($mue_array, $local_id, $validate_mode_on){
            global $mydb;
            
            $query_get_ungraded_data = queryDB("SELECT * FROM posted_exams WHERE exam_id='$local_id'");
            if ($query_get_ungraded_data->num_rows == 0) { // nothing found.
                return throwError("Error: ungraded exam with id=$local_id not found in posted exams list.");
            } // using query to get exam's name and stable name.
            $exam_data = $query_get_ungraded_data->fetch_assoc();
            $exam_name = $exam_data['exam_name'];
            $exam_table_name = $exam_data['exam_table_name'];
            
            foreach ($mue_array as $question_block){
                $question_id = -1; $points = -1; $score = -1; $response_data = -1; $comment = -1; $grade_tuple = NULL;
                foreach ($question_block as $k=>$v){
                    // if($k!='id'&&$k!='response'&&$k!='comment'&&$k!='psp'){
                    //     throwError("test: $k");
                    //     exit();
                    // }
                    if($k == 'grade_tuple') continue;

                    // this if-block checks for point or score labels and populates $points and $score respectively.
                    if($k == 'point score pair' || $k == 'point_score_pair' || $k == 'psp') {
                        
                        $psp = json_decode($v);
                        foreach ($psp as $k2=>$v2){
                            $p_or_s = parse_exam_question_data($k2, $v2);
                            if(!$p_or_s){
                                return throwError("ERROR: Parsing error in 'mue'->parse_exam_question_data().");
                            } else {
                                $label_type = $p_or_s['type'];
                                $label_value = $p_or_s['value'];
                                if ( $label_type == 'p') $points = $label_value;
                                else $score = $label_value;
                            }
                        }
                        continue;
                    }

                    // question id, response data, local comment
                    $q_res_or_comment = parse_exam_question_data($k, $v);
                    if (!$q_res_or_comment){
                        throwError("ERROR: Parsing error in 'mue'->parse_exam_question_data().");
                        exit();
                    } else {
                        $label_type = $q_res_or_comment['type'];
                        $label_value = $q_res_or_comment['value'];
                        switch($label_type){
                            case 'q':
                                $question_id = $label_value;
                                break;
                            case 'd':
                                $response_data = $label_value;
                                break;
                            case 'c':
                                $comment = $label_value;
                                break;
                            default:
                                throwError("did i miss something? ");
                                exit();
                        }
                    }
                }
                if ($validate_mode_on) continue;
                
                // echo "\np: $points\nq: $question_id \ns: $score \nd: $response_data \nc: $comment \n";
                // echo " ============= ";

                $point_score_array = array(
                    "max_points"=>$points,
                    "score"=>$score
                );
                $point_score_pair = json_encode($point_score_array);

                
                $query_add_exam_data = queryDB(
                    "INSERT into $exam_table_name (question_id, response_data, point_score_pair, question_comment, grade_tuple)
                    VALUES ('$question_id', '$response_data', '$point_score_pair', '$comment', '$grade_tuple')"
                );
                if (!$query_add_exam_data){
                    $k= mysqli_error($mydb);
                    return throwError("ERROR: $k");
                }

                if ($mydb->affected_rows == 0) {
                    throwError("Could not add exam with name='$exam_name' to posted exams list.");
                    exit();
                }
            }
            return 1;
        }

        function repopulate_ungraded_exam($mue_array, $local_id){
            $populate_mode_on = 0; //simple function. just for the name.
            validate_mue_data_array($mue_array, $local_id, $populate_mode_on);
        }

        function search_question_for_keywords($question_id, $fqbk_keyword, $fqbk_keyword_delimiter){
            
            // this simply needs to return the usual ret.

            $query_getQuestion = queryDB("SELECT * FROM question_bank where id='$question_id'");

            // echo $question_id;
            if ($query_getQuestion->num_rows > 0) {
                $keyword_found = false;
                $found_keywords = array();

                $row = $query_getQuestion->fetch_assoc();

                if($fqbk_keyword_delimiter=="")$fqbk_keyword_delimiter=" ";
                    
                $keywords = explode($fqbk_keyword_delimiter, $fqbk_keyword);
                                
                foreach ($keywords as $keyword){
                    
                    
                    if ($fqbk_keyword==''){
                        $keyword_found = true;
                        $arr = array(
                            "keyword"=>$keyword,
                            "id"=>$row["id"],
                            "question_name"=>$row["name"],
                            "attribute"=>"any"
                        );
                        return $arr;
                        // array_push($found_keywords, $arr); // above: commenting out greedy search algo. nested array is incompat with frontend.
                        // break;
                    }

                    // echo '_in:';
                    $ret = find_word_in_phrase($keyword, $row["name"]);
                    // echo $ret;
                    // echo ':out_';
                    if ($ret==1) {
                        $keyword_found = true;
                        $arr = array(
                            "keyword"=>$keyword,
                            "id"=>$row["id"],
                            "question_name"=>$row["name"],
                            "attribute"=>"name"
                        );
                        return $arr;
                        // array_push($found_keywords, $arr);//see above
                    }

                    // echo '_in:';
                    $ret = find_word_in_phrase($keyword, $row["description"]);
                    // echo $ret;
                    // echo ':out_';
                    if ($ret==1) {
                        $keyword_found = true;
                        $arr = array(
                            "keyword"=>$keyword,
                            "id"=>$row["id"],
                            "question_name"=>$row["name"],
                            "attribute"=>"description"
                        );
                        return $arr;
                        // array_push($found_keywords, $arr);//see above
                    }

                    // echo '_in:';
                    $ret = find_word_in_phrase($keyword, $row["function_name"]);
                    // echo $ret;
                    // echo ':out_';
                    if ($ret==1) {
                        $keyword_found = true;
                        $arr = array(
                            "keyword"=>$keyword,
                            "id"=>$row["id"],
                            "question_name"=>$row["name"],
                            "attribute"=>"function_name"
                        );
                        return $arr;
                        // array_push($found_keywords, $arr);//see above
                    }

                    // echo '_in:';
                    $ret = find_word_in_phrase($keyword, $row["topic"]);
                    // echo $ret;
                    // echo ':out_';
                    if ($ret==1) {
                        $keyword_found = true;
                        $arr = array(
                            "keyword"=>$keyword,
                            "id"=>$row["id"],
                            "question_name"=>$row["name"],
                            "attribute"=>"topic"
                        );
                        return $arr;
                        // array_push($found_keywords, $arr);//see above
                    }                            
                }

                // if ($keyword_found){
                //     // return $found_keywords;
                //     return 1;
                // } else {
                    return 0;
                // }
            } else {
                //throw an exception -- i gave it an id and yet it says the question doesn't exist?
                return -1;
            }
        }

        
        function find_word_in_phrase($word, $phrase){

            // echo $word;
            // echo $phrase;
            if ($word=="" || $phrase=="") {
                // echo "not 1 found";   
                return 0;
            }

            if (strpos($phrase, $word) !== false) {
                $start = strpos($phrase, $word);
                
                $before=$start-1;
                $after=$start+strlen($word);
                
                $item_at_before = $phrase[$before];
                $item_at_after = $phrase[$after];

                if($before !== -1){ // if prev char == out of bounds.
                    if(IntlChar::isalpha($item_at_before)){
                        //bad
                        // echo "not 2 found";
                        return 0;
                    }
                }// else, it's out of bounds. safe cause at start of phrase
                if($after < strlen($phrase)){// if char after word == out of bounds
                    if(IntlChar::isalpha($item_at_after)){
                        //bad
                        // echo "not 3 found";
                        return 0;
                    }
                }//out of bounds. safe, cause it's at the end of the phase.

                // echo "found(4)";
                return 1;
            } else {
                // echo "not 5 found";
                return 0;
            }
        }


        $mydb->close();
        
    ?>