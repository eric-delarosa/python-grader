<?php
// Eric De La Rosa
// CS490-004 Alpha Controller
// edd8@njit.edu
// Test of sshfs

//echo "here in middle";

$mode = $_POST['mode'];
$url_back = "https://afsaccess4.njit.edu/~abh7/";

switch($mode){
    // USER MANAGMENT
    case "login-user":
        $name = $_POST['username'];
        $pass = $_POST['password'];
        $fields = array(
            'mode'      => $mode,
            'username'  => $name,
            'password'  => $pass,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "set-pass":
        $name = $_POST['username'];
        $pass = $_POST['password'];
        $fields = array(
            'mode'      => $mode,
            'username'  => $name,
            'password'  => $pass,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "create-user":
        $name = $_POST['username'];
        $pass = $_POST['password'];
        $role = $_POST['role'];
        $fields = array(
            'mode'      => $mode,
            'username'  => $name,
            'password'  => $pass,
            'role'      => $role,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "delete-user":
        $name = $_POST['username'];
        $fields = array(
            'username'  => $name,
            'mode'      => $mode,
        );
        $response = curlPost($url_back, $fields);
        break;

    // OBSOLETE API CALLS, ONLY FOR TESTING -----------------------------------
    case "send-data":
        $name = $_POST['username'];
        $pass = $_POST['password'];
        $data = $_POST['data'];
        $fields = array(
            'username'  => $name,
            'password'  => $pass,
            'mode'      => $mode,
            'data'      => $data,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "test-grader":
        $code = $_POST['code'];
        $tc   = $_POST['testcase'];
        $sol  = $_POST['solution'];
        var_dump($code);
        var_dump($tc);
        var_dump($sol);
        $vale = python_checker($code, $tc, $sol);
        var_dump($vale);
        if ($vale[0] == 43){
            echo "system returned 43" . PHP_EOL;
            echo "Output from code " . $vale[1][0] . PHP_EOL;
            echo "Grader Succeeded" . PHP_EOL;
        }
        else{
            echo "Grader Failed" . PHP_EOL;
        }
        break;

// -- END --------------------------------------------------------------------

// -- ACTIVE/FUNCTIONAL API CALLS --------------------------------------------

    case "add-question":
        $name   = $_POST['username'];
        $pass   = $_POST['password'];
        $desc   = $_POST['desc'];
        $qname  = $_POST['question-name'];
        /* alias */ if($qname=="") $qname = $_POST['name'];
        $g_list  = $_POST['gradable-list'];
        $diff   = $_POST['difficulty'];
        $topic  = $_POST['topic'];
        $fields = array(
            'username'      => $name,
            'mode'          => $mode,
            'password'      => $pass,
            'desc'          => $desc,
            'name'          => $qname,
            'gradable-list' => $g_list,
            'difficulty'    => $diff,
            'topic'         => $topic,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "get-question":
        $response = get_question($mode,$url_back);
        break;

    case "list-questions":
        $fields = array(
            'mode' => $mode,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "delete-question":
        $name = $_POST['username'];
        $pass = $_POST['password'];
        $qid  = $_POST['id'];
        $fields = array(
            'username'  => $name,
            'mode'      => $mode,
            'id'        => $qid,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "filter":
        $topic = $_POST['topic'];
        $difficulty = $_POST['difficulty'];
        $keyword = $_POST['keyword'];
        $fields = array(
            'mode' => $mode,
            'topic' => $topic,
            'difficulty' => $difficulty,
            'keyword' => $keyword,
        );
        $response = curlPost($url_back, $fields);
        break;

    // -- EXAM FUNCTIONS -------------------------------------------------
    case "cue": // create unposted exam
        $name   = $_POST['username'];
        $pass   = $_POST['password'];
        $ename  = $_POST['exam-name'];
        /* alias */ if($ename=="") $ename = $_POST['name'];
        $qpp    = $_POST['questionID-point-pairs'];
        /* alias */ if($qpp=="") $qpp = $_POST['qpp'];
        $fields = array(
            'mode'      => $mode,
            'username'  => $name,
            'password'  => $pass,
            'exam-name' => $ename,
            'qpp'       => $qpp,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "pue": // post ungraded exam
        $name   = $_POST['username'];
        $pass   = $_POST['password'];
        $ename  = $_POST['name'];
        $eid    = $_POST['global-id'];
        $gcomm  = $_POST['global-comment'];
        /* alias */ if($gcomm=="") $gcomm = $_POST["exam-comment"];
        $tgrade = $_POST['total-grade'];
        /* alias */ if($tgrade=="") $tgrade= $_POST["exam-grade"];
        if($gcomm == ""){
            $fields = array(
                'mode'      => $mode,
                'username'  => $name,
                'password'  => $pass,
                'name'      => $ename,
                'global-id' => $eid,
                'total-grade' => $tgrade,
            );
        } else {
            $fields = array(
                'mode'      => $mode,
                'username'  => $name,
                'password'  => $pass,
                'name'      => $ename,
                'global-id' => $eid,
                'global-comment' => $gcomm,
                'total-grade' => $tgrade,
            );
        }
        $response = curlPost($url_back, $fields);
        break;

    case "pge": // post graded exam
        $name   = $_POST['username'];
        $pass   = $_POST['password'];
        $ename  = $_POST['name'];
        $eid    = $_POST['global-id'];
        $gtuple  = $_POST['grade-tuple'];
        $fields = array(
            'mode'      => $mode,
            'username'  => $name,
            'password'  => $pass,
            'name'      => $ename,
            'global-id' => $eid,
            'grade-tuple'     => $gtuple,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "get-exam":
        $response = get_exam($mode, $url_back);
        break;

    case "delete-exam":
        $name   = $_POST['username'];
        $pass   = $_POST['password'];
        $eid    = $_POST['global-id'];
        $fields = array(
            'mode'      => $mode,
            'username'  => $name,
            'password'  => $pass,
            'global-id' => $eid,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "list-exams":
        $fields = array(
            'mode' => $mode,
        );
        $response = curlPost($url_back, $fields);
        break;

    case "autograde":

        $role = $_POST['role'];
        if ($role != 'teacher'){
            throwError("Error: Illegal access to Autograder");
            exit();
        }
        else{
            $qr = $_POST['qr_pair']; // Question Response pair
            $qr = json_decode($qr, true);
            $qid = $qr['id']; // Question ID
            $q_resp = $qr['response']; // Student response data to question
            $q_max_points = $qr['points']; // Questions points worth
            $name = $_POST['username'];
            $pass = $_POST['password'];
            $fields = array(
                'username'  => $name,
                'mode'      => 'get-question',
                'id'        => $qid,
                'password'  => $pass,
            );

            $gq_info = curlPost_echofree($url_back, $fields); // Result of get-question API call

            $gq_info = json_decode($gq_info, true);
            $gq_info = json_decode($gq_info, true);

            $g_list = $gq_info["gradable_list"];

            $g_list = json_decode($g_list, true);
            $func_name = $g_list["function-name"]['expected'];
            $func_name = trim($func_name, "()");
            $constraint = $g_list["constraint"]['expected'];

            // --- FUNCTION NAME CHECK -------------------------------
            // check for function name in response data, add comment
            //$fname_pos = strpos($q_resp, $func_name);
            $fname_given = get_between($q_resp,'def ','(');
            // TO-DO: Get the score to relate to total points of question
            $fscore = 0;
            if($fname_given == $func_name){
                // Function name exists in the response data
                $fscore = 0;
            }
            else {
                // If function name is wrong, deduct points
                $fscore = -1;
                // Replace wrong function name with propoer one to run testcases
                $q_resp = str_replace($fname_given, $func_name, $q_resp);
            }

            $func_arr = array(
                'expected' => $func_name,
                'given' => $fname_given,
                'score' => $fscore,
            );
            // --- CONSTRAINT CHECK ----------------------------------
            // check for constraint in response data, add comment
            $const_given = '';
            $cscore = 0;
            if ($constraint == "") {
                $const_given = $constraint;
                $cscore -= 0;
            }
            else {
                // check if constraint is recursion
                if ($constraint == "recursion") {
                    // check if function call exists in the string in a different position then definition
                    if (strpos($q_resp, $fname_given) !== strpos($q_resp, $fname_given, -1)){
                        //echo "Constraint Correct";
                        $const_given = $constraint;
                        $cscore -= 0;
                    }
                    // check if recursion is incorrect
                    else{
                        $const_given = "-";
                        $cscore -= 1;
                        $g_list['constraint']['comment'] .= " Constraint incorrect.";
                    }
                }
                // Constraint check for/while
                else if ($constraint == "for" || $constraint == "while") {
                    // Constraint exists and is in the response data
                    $const_pos = strpos($q_resp, $constraint);
                    // Correct
                    if ($const_pos) {
                        //echo "Constraint Correct";
                        $const_given = $constraint;
                    } // incorrect
                    else {
                        // If constraint does not exist in response, deduct points
                        //echo "constraint incorrect" . PHP_EOL;
                        $cscore -= 1;
                        $const_given = "-";;
                    }
                }
                else{throwError("Unaccepted Constraint Given"); exit();}
            }

            $const_arr = array(
                'expected' => $constraint,
                'given' => $const_given,
                'score' => $cscore,
            );

            // --- PYTHON CODE CHECK ---------------------------------
            // Run python grader for each question testcase, add comment for each testcase
            //$g_list = json_decode($g_list, true);
            $tcsp_total_score = 0;
            $tcsp_arr = array();
            if($g_list == NULL){throwError("Error, no testcases");exit();}
            else{
                $q_resp = str_replace($fname_given, $func_name, $q_resp);
                $q_tcsp = $g_list['tcsp-list'];
                foreach ($q_tcsp as $tc => $tc_list){
                    $tcsp_score = 0;
                    $tc_param = get_between($tc, '(',')');
                    $tcsp_given = python_checker($q_resp, $tc_param, $func_name);
                    $tcsp_given = trim($tcsp_given);
                    if($tcsp_given == $tc_list['expected']){
                        $tcsp_score = 0;
                        $tcsp_total_score -= 0;
                    }
                    else{
                        $tcsp_score = -1;
                        $tcsp_total_score -= 1;
                    }
                    $tc_arr = array(
                        'expected'  => $tc_list['expected'],
                        'given'     => $tcsp_given,
                        'score'     => $tcsp_score,
                    );
                    $tcsp_arr = array_push_assoc($tcsp_arr, $tc, $tc_arr);
                }
            }

            $grade_data = array(
                'fname' => $func_arr, // add func array (expected, given, score)
                'constraint' => $const_arr, // add constraint array (expected, given, score)
                'tcsp' => $tcsp_arr, // add tcsp array (expected, given, score)
            );
            $mid_response = array(
                'status' => 1, // status good
                'grade_data' => $grade_data, // grade data array
            );
            $mid_response = json_encode($mid_response);
            echo $mid_response;

            break;
        }
        break;


// Grade-exam will call get-exam first to get the qpp
// and function name and then the data will be processed
// to determine a grade value which will then send a new
// post to the back with the grade value to store.
    case "mue":
        $role = $_POST['role']; // Student or teacher
        $submitted = $_POST['submitted-exam'];
        if ($role == "student"){ // student call for when exam is taken
            // Curl mue
            $name   = $_POST['username'];
            $pass   = $_POST['password'];
            $eid    = $_POST['global-id'];
            $mue_data = $_POST['mue-data'];
            $gcomm = $_POST['global-comment'];
            $fields_st = array(
                'mode'      => $mode,
                'username'  => $name,
                'password'  => $pass,
                'global-id' => $eid,
                'mue-data' => $mue_data,
                'submitted-exam' => $submitted,
            );
            $response = curlPost($url_back, $fields_st);
            break;
        }
        else {throwError("Illegal Access to mue"); break;}

    default:
        throwError("Error! No accepted mode declared.");
        break;
}

// Function that runs python script given str code and testcases
function python_checker($codeIn, $tcIn, $fname){
    $output = null;
    $ret_code = null;

    $code_arch = "{{func_def}}\r\n\r\nprint({{func_name}}(params}}))\r\n\r\n";
    $base_script = str_replace("{{func_def}}", $codeIn, $code_arch);
    $base_script = str_replace("{{func_name}}", $fname, $base_script);

    $grader_script_name = "grader.py";


    $file = fopen($grader_script_name, "w");
    shell_exec("chmod 777 grader.py");

    $script_params = str_replace("params}}", $tcIn, $base_script);

    $final_grader_script = $script_params;

    fwrite($file, $final_grader_script);

    $cmd = "python $grader_script_name";
    $output = shell_exec($cmd);


    fclose($file);
    shell_exec("rm grader.py");

    return $output;
}

// fucntion to get value in string in between 2 characters
function get_between($input, $start, $end)
{
    return substr($input, strlen($start)+strpos($input, $start), (strlen($input) - strpos($input, $end))*(-1));
}

function curlPost($url, $dataStream){
    $fields_string="";
    foreach($dataStream as $key=>$value) { $fields_string .= $key.'='.$value.'&';}
    rtrim($fields_string, '&');

    $curl_back = curl_init();
    curl_setopt($curl_back, CURLOPT_URL, $url);
    curl_setopt($curl_back, CURLOPT_POST, count($dataStream));
    curl_setopt($curl_back, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($curl_back, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl_back);

    if($response === FALSE){
        die(curl_error($curl_back));
    }
    echo json_encode($response);
    curl_close($curl_back);

    return $response;
}

function curlPost_echofree($url, $dataStream){
    $fields_string="";
    foreach($dataStream as $key=>$value) { $fields_string .= $key.'='.$value.'&';}
    rtrim($fields_string, '&');

    $curl_back = curl_init();
    curl_setopt($curl_back, CURLOPT_URL, $url);
    curl_setopt($curl_back, CURLOPT_POST, count($dataStream));
    curl_setopt($curl_back, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($curl_back, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl_back);

    if($response === FALSE){
        die(curl_error($curl_back));
    }
    $response = json_encode($response);
    curl_close($curl_back);

    return $response;
}

function get_question($mode, $url_back){
    $name = $_POST['username'];
    $pass = $_POST['password'];
    $qid  = $_POST['id'];
    $fields = array(
        'username'  => $name,
        'mode'      => $mode,
        'id'        => $qid,
        'password'  => $pass,
    );
    return curlPost($url_back, $fields);
}

function get_exam($mode, $url_back){
    $name   = $_POST['username'];
    $pass   = $_POST['password'];
    $eid    = $_POST['global-id'];
    $fields = array(
        'mode'      => $mode,
        'username'  => $name,
        'password'  => $pass,
        'global-id' => $eid,
    );
    return curlPost($url_back, $fields);
}

function throwError($msg){
    $response = array("status"=>0,"desc"=>$msg);
    echo json_encode($response);
}

// Utility function to push a key value pair into an array
function array_push_assoc($array, $key, $value){
    $array[$key] = $value;
    return $array;
}

?>